
// import packages -----------------------------------

    const Discord = require("discord.js"); // import discord.js
    const fs = require("fs"); // import local filesystem
    // const {Config} = require("dotenv-flow"); // import dotenv
    require("dotenv-flow").config(); // import dotenv
// ---------------------------------------------------

// environment variables -----------------------------

    const Config = {
        token: process.env.TOKEN,
        owner: process.env.OWNER
    }

// ---------------------------------------------------

// object constants ----------------------------------

    const Client = new Discord.Client();
    Client.commands = new Discord.Collection(); // creates a collection of discord commands into Client.commands

    const botPrefix = '&';

// ---------------------------------------------------

// commands setup ------------------------------------

    const CommandFiles = fs.readdirSync("./commands/").filter(file => file.endsWith(".js"));
    // filters all files to scan only .js files

    for(const file of CommandFiles){

        const Command = require(`./commands/${file}`);
        Client.commands.set(Command.name, Command);
        // sets each command as a constant

    } // for loop to set all commands in the commands folder

// ----------------------------------------------------

// Client/bot methods ---------------------------------

    Client.once("ready", () => {
        console.log("Cinder is online!");
    }); // says if discord bot is online

    Client.on("message", message => {

        if(!message.content.startsWith(botPrefix) || message.author.bot) return; // returns nothing if no prefix is present

        const args = message.content.slice(botPrefix.length).split(" "); // splicing command prefix
        const command = args.shift().toLowerCase(); // makes the command not case-sensitive by converting text to lowecase always

        if(command === "ping"){
            Client.commands.get('ping').execute(message,args);
        }
        else if(command === "owo"){
            Client.commands.get('owo').execute(message,args);
        }
        else if(command === "uwu"){
            Client.commands.get('uwu').execute(message,args);
        }

    }); // stuff it does when the bot is online

// -----------------------------------------------------


Client.login(Config.token);